use std::io;

fn add2(x: i32, y: i32) -> i32 {
    x + y
}


fn main() {
    let mut input_line = String::new();

    io::stdin().read_line(&mut input_line).expect("ERROR");
    //println!("{}", input_line);
    let x: i32 = input_line.trim().parse().expect("Input not an integer");

    input_line = String::new();
    io::stdin().read_line(&mut input_line).expect("ERROR");
    //println!("{}", input_line);
    let y: i32 = input_line.trim().parse().expect("Input not an integer");

    println!("Hello, world!");
    println!("{}", add2(x, y));
}
